# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Calendar Holiday Germany Bavaria',
    'name_de_DE': 'Kalender Deutsche Feiertage Bayern',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Provides calendar templates for holidays specific for
      German state Bavaria
    ''',
    'description_de_DE': '''
    - Stellt Kalendervorlagen für Feiertage des deutschen Bundeslandes Bayern
      zur Verfügung.
    ''',
    'depends': [
        'calendar_template'
    ],
    'xml': [
        'holiday_de_by.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
